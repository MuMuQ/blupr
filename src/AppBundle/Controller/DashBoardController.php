<?php
/**
 * Created by PhpStorm.
 * User: dominik
 * Date: 16.05.16
 * Time: 16:06
 */

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class DashBoardController extends Controller
{
    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function dashboardAction(Request $request)
    {

        return $this->render(
            'dashboard.html.twig',
            array(
                // last username entered by the user
                'last_username' => "dashboard",
                'error'         => "dashboard",
            )
        );
    }
}